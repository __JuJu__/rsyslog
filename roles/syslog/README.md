Role Name
=========

This role is deploying a rsyslog server

Requirements
------------

Centos minimal 7 2009 is the targeted OS

Role Variables
--------------

Since this is really easy, all variables are store within task/main.yaml

Dependencies
------------

No  dependencies


License
-------

BSD
