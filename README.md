# Rsyslog server configuration

The aim of this playbook is to deploy remotedly a Rsyslog server

## Configuration

By default, I have choosen to dynamically write incoming logs to a dedicated folder this way:

```
/var/log/remote/IPADDRESS/file.log
```
where:
- ***IPADRESSE*** is the IP address of the incoming traffic

- and ***file.log*** is the name of the log file that has been forwarded.
